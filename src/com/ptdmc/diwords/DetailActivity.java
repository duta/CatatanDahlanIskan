package com.ptdmc.diwords;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.R.integer;
import android.R.string;
import android.app.ProgressDialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

public class DetailActivity extends ActionBarActivity
{	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		if (savedInstanceState == null)
		{
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		int postID;
		// get intent
		Bundle extras = getIntent().getExtras();
		postID = extras.getInt("post_id");
		new GetBlogDetailsTask(postID).execute("");
	}

	public static final class PlaceholderFragment extends Fragment
	{
		@Override
		public View onCreateView(final LayoutInflater inflater,
				final ViewGroup container, final Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_detail,
					container, false);
			return rootView;
		}
	}

//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event)
//	{
//		if (keyCode == KeyEvent.KEYCODE_BACK)
//		{
//			onBackPressed();
//		}
//		return super.onKeyDown(keyCode, event);
//	}
//
//	public void onBackPressed()
//	{
//		finish();
//		return;
//	}

	private class GetBlogDetailsTask extends AsyncTask<String, Integer, Post>
	{
		private int postID;
		private String urlBlog = "https://public-api.wordpress.com/rest/v1.1/sites/3371341/posts/"; // +
																									// postID
		private ProgressDialog Dialog = new ProgressDialog(DetailActivity.this);
		
		public GetBlogDetailsTask(int postID)
		{
			this.postID = postID;
		}
		
		@Override
		protected void onPreExecute()
		{
			// TODO Auto-generated method stub
			super.onPreExecute();
			Dialog.setMessage("Loading ...");
	        Dialog.show();
		}

		@Override
		protected Post doInBackground(String... params)
		{
			Post result = null;
			StringBuilder sb = new StringBuilder();

			try
			{
				URL url = new URL(urlBlog + postID);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200)
				{
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
						(conn.getInputStream())));

				String output;

				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null)
				{
					sb.append(output);
				}
				JSONObject j = new JSONObject(sb.toString());
				sb = null;
				conn.disconnect();

				Gson mGson = new Gson();
				result = mGson.fromJson(j.toString(), new TypeToken<Post>()
				{}.getType());
			}
			catch (Exception e)
			{
				Log.e("Koneksi ke wordpress", e.getMessage());
			}
			return result;
		}

		@Override
		protected void onPostExecute(Post result)
		{			
			super.onPostExecute(result);
			Dialog.dismiss();
			if (result == null)
			{}
			else
			{
				final TextView textview = (TextView) findViewById(R.id.textview_post);
				Spanned sp = Html.fromHtml("&nbsp;&nbsp;<h3>" + result.title
						+ "</h3></br>" + result.content);
				textview.setText(sp);
				textview.setMovementMethod(new ScrollingMovementMethod());
				
			}
		}

	}

}
