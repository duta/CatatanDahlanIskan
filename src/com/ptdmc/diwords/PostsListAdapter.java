package com.ptdmc.diwords;

import java.util.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.ptdmc.utils.*;

public class PostsListAdapter extends ArrayAdapter<Post>
{
	private final Context context;
	private final ArrayList<Post> values;

	public PostsListAdapter(Context context, ArrayList<Post> values)
	{
		super(context, R.layout.post_line, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.post_line, parent, false);
		TextView headLine = (TextView) rowView.findViewById(R.id.firstLine);
		TextView secondLine = (TextView) rowView.findViewById(R.id.secondLine);
		try
		{
			headLine.setText(HTMLDecoder.decode(((Post)values.get(position)).title));
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			try
			{
				Date tglPost = formatter.parse(((Post)values.get(position)).date);
				secondLine.setText(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(tglPost));
			}
			catch (Exception exDtFmt)
			{
				Log.e("Date formatting error", exDtFmt.getMessage());
				secondLine.setText(((Post)values.get(position)).date.substring(0,10));
			}		
			
		}
		catch (Exception ex)
		{
			Log.e("Print to listview",  ex.getMessage() + " position is " + position);
		}
		return rowView;
	}
}
