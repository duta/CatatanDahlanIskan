package com.ptdmc.diwords;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.R.anim;
import android.R.bool;
import android.R.integer;
import android.R.string;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.*;
import com.google.gson.reflect.*;
import com.ptdmc.diwords.components.*;

import org.apache.http.client.utils.URIBuilder;

public class MainActivity extends ActionBarActivity
{

	private Gson mGson = new Gson();
	public int jmlPosts = 0;	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null)
		{
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	
		if (jmlPosts == 0) {
			Toast.makeText(this, "Download feed from dahlaniskan.wordpress.com",
					Toast.LENGTH_SHORT).show();	
		}
	}	

	@Override
	protected void onStop()
	{
		Log.i("onStop", "MainActivity Stopped");
		super.onStop();
	}
	
	@Override
	protected void onPause()
	{
		Log.i("onPause", "MainActivity Pause");
		super.onPause();
	}
	
	@Override
	protected void onDestroy()
	{
		Log.i("onDestroy", "MainActivity Destroy");
		super.onDestroy();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();		
		if (jmlPosts == 0) {
			// attach quick return menus
			ListView listData = (ListView) findViewById(R.id.listview);
			((QuickReturnFrameLayout) findViewById(R.id.frame)).attach(listData);

			// start download feed
			new GetBlogTitlesTask(10, 0,"").execute("");	
		}		
	};	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		// Associate searchable configuration with the SearchView
	    SearchManager searchManager =
	           (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView =
	            (SearchView) menu.findItem(R.id.search).getActionView();
	    searchView.setSearchableInfo(
	            searchManager.getSearchableInfo(getComponentName()));
	    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.i("onQueryTextSubmit", s);                
                new GetBlogTitlesTask(10, 0, s).execute("");
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                Log.i("onQueryTextChange", s);
//            	if (s == ""){
//            		new GetBlogTitlesTask(10, 0, "").execute("");
//            	}
                return false;
            }
        });
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
//		Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();;
		if (id == R.id.action_home)
		{
			new GetBlogTitlesTask(10, 0, "").execute("");
		}
		return super.onOptionsItemSelected(item);
	}

	public void viewDetail(final Post post)
	{
		Intent intent = new Intent(this, DetailActivity.class);
		intent.putExtra("post_id", post.ID);
		startActivity(intent);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment
	{

		public PlaceholderFragment()
		{}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private class GetBlogTitlesTask extends
			AsyncTask<String, Integer, JSONArray>
	{		
		private int itemperview, offset;
		private String searchWhat = "";
		private URIBuilder urib;
		private String urlBlog = "https://public-api.wordpress.com/rest/v1.1/sites/3371341/posts/"; // &number=10&offset=0
		final Button buttonNext = (Button) findViewById(R.id.buttonNext);
		final Button buttonPrev = (Button) findViewById(R.id.buttonPrev);
		private ProgressDialog Dialog = new ProgressDialog(MainActivity.this);

		public GetBlogTitlesTask(int itemperview, int offset, String searchWhat)
		{
			this.itemperview = itemperview;
			this.offset = offset;
			this.searchWhat = searchWhat;
			try
			{
				urib = new URIBuilder(urlBlog);
			}
			catch (URISyntaxException e1)
			{
				Log.e("getblogtitletask urbuilder build exception", e1.getMessage());
			}
			urib.setParameter("number", String.valueOf(itemperview))
			.setParameter("offset", String.valueOf(offset));			
		}

		@Override
		protected JSONArray doInBackground(String... params)
		{
			StringBuilder sb = new StringBuilder();
			JSONArray posts = null;
			try
			{
				String urlToExecute = "";				
				if (searchWhat != ""){
					urib.addParameter("search", searchWhat);
				}
				urlToExecute = urib.toString() + "&fields=ID,title,date";
				Log.i("urltoexecute", urlToExecute);
				URL url = new URL(urlToExecute);
				
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200)
				{
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
						(conn.getInputStream())));

				String output;

				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null)
				{
					sb.append(output);
				}
				jmlPosts = new JSONObject(sb.toString())
						.getInt("found");
								
				posts = new JSONObject(sb.toString()).getJSONArray("posts");
				sb = null;
				conn.disconnect();
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Log.e("Koneksi ke wordpress", e.getMessage());
				return null;
			}
			return posts;
		}	
		
		@Override
		protected void onPostExecute(JSONArray result)
		{
			Dialog.dismiss();
			// TODO Auto-generated method stub
			if (result == null)
			{
				Toast.makeText(MainActivity.this, "No data from dahlaniskan.wordpress.com",
						Toast.LENGTH_LONG).show();	
			}
			else
			{
				final ArrayList<Post> items = mGson.fromJson(result.toString(),
						new TypeToken<ArrayList<Post>>()
						{}.getType());
				PostsListAdapter adapter = new PostsListAdapter(MainActivity.this, items);

				final ListView listview = (ListView) findViewById(R.id.listview);

				listview.setAdapter(adapter);

				listview.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id)
					{
						// Toast.makeText(
						// MainActivity.this,
						// "You selected : " + items.get(position).title,
						// Toast.LENGTH_SHORT).show();
						viewDetail(items.get(position));
					}
				});

				OnClickListener buttonClickNext = new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						offset += itemperview;
						buttonPrevNextGone();
						new GetBlogTitlesTask(itemperview, offset, searchWhat)
								.execute("");
					}
				};
				buttonNext.setOnClickListener(buttonClickNext);
				OnClickListener buttonClickPrev = new OnClickListener()
				{

					@Override
					public void onClick(View arg0)
					{
						offset -= itemperview;
						buttonPrevNextGone();
						new GetBlogTitlesTask(itemperview, offset,searchWhat)
								.execute("");
					}
				};
				buttonPrev.setOnClickListener(buttonClickPrev);

			}

			super.onPostExecute(result);
			buttonPrevNextVisible(offset);
		}

		private void buttonPrevNextVisible(int offset)
		{
			int lastPage = jmlPosts /10;
			boolean isLastPage = (offset + 10) / 10 > lastPage ? true : false; 
			buttonNext
					.setVisibility(isLastPage ? android.view.View.GONE
							: android.view.View.VISIBLE);
			Log.i("button next", ""+offset);
			Log.i("button next","offset % 10 is " + String.valueOf(offset % 10) + " and jmlPosts % 10 is " + String.valueOf(jmlPosts % 10));
			buttonPrev.setVisibility(offset == 0 ? android.view.View.GONE
					: android.view.View.VISIBLE);
		}

		private void buttonPrevNextGone()
		{
			buttonNext.setVisibility(android.view.View.GONE);
			buttonPrev.setVisibility(android.view.View.GONE);
		}

		@Override
		protected void onPreExecute()
		{
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			Dialog.setMessage("Loading ...");
	        Dialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values)
		{
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

	}
}
